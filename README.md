# flakes equivalent of `nix-shell`

## `nix-shell -p`

The flakes equivalent of `nix-shell -p hello` is `nix shell "nixpkgs#hello"`,
producing a $SHELL shell where the 'hello' command is available.

## `nix-shell -A`

The flakes equivalent of `nix-shell -A hello` is `nix develop "nixpkgs#hello"`,
producing an interactive bash shell with everything present used to build the
`hello` package, such as `unpackPhase`, useful to debug the derivation.

You can add stuff to `devShell`(`s`) to control what's available on
this shell.

## `nix-shell`

However, it is a bit unclear what the equivalent of just `nix-shell` is:
when invoked like that case `nix-shell` that would read `shell.nix` to
produce a shell with the specified packages available, often defined
with `mkShell`. This is useful when you want to enter a shell with a
bunch of stuff, possibly with some particular overrides or other specifics.

I don't think there is currently a good convention on how to get that
behaviour. Some people use `nix develop` with `mkShell` and `-c $SHELL`
(https://nixos.wiki/wiki/Flakes#Super_fast_nix-shell) but that seems kinda
abusing `nix develop`?

This repo experiments with using an `mkShell`-like derivation that produces
a script that enters the $SHELL and can be run as an 'app'. Not sure what
to make of it yet :)

You can run it with `nix run .#myShell`. The idea is that perhaps `nix shell`
without further parameters could default to `nix run .#shell` or something.
Needs more thought ;)
