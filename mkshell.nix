{ lib, stdenv, buildEnv, bash }:

# A variation on 'mkShell' for use with flakes:
#
# this derivation takes the same parameters as
# the 'regular' mkShell, but instead of producing
# a 'special' derivation to be used with nix-shell, 
# it 'just' produces a script that when executed
# enters $SHELL with the appropriate environment

{ name ? "nix-shell"
, # a list of packages to add to the shell environment
  packages ? [ ]
, # propagate all the inputs from the given derivations
  inputsFrom ? [ ]
, buildInputs ? [ ]
, nativeBuildInputs ? [ ]
, propagatedBuildInputs ? [ ]
, propagatedNativeBuildInputs ? [ ]
, ...
}@attrs:
let
  mergeInputs = name:
    (attrs.${name} or [ ]) ++
    (lib.subtractLists inputsFrom (lib.flatten (lib.catAttrs name inputsFrom)));

  rest = builtins.removeAttrs attrs [
    "name"
    "packages"
    "inputsFrom"
    "buildInputs"
    "nativeBuildInputs"
    "propagatedBuildInputs"
    "propagatedNativeBuildInputs"
    "shellHook"
  ];
in

stdenv.mkDerivation ({
  inherit name;

  buildInputs = mergeInputs "buildInputs";
  nativeBuildInputs = packages ++ (mergeInputs "nativeBuildInputs");
  propagatedBuildInputs = mergeInputs "propagatedBuildInputs";
  propagatedNativeBuildInputs = mergeInputs "propagatedNativeBuildInputs";

  shellHook = lib.concatStringsSep "\n" (lib.catAttrs "shellHook"
    (lib.reverseList inputsFrom ++ [ attrs ]));

  phases = [ "buildPhase" ];

  buildPhase = ''
    echo "#!${bash}/bin/bash" > $out
    echo >> $out

    # Record all build inputs as runtime dependencies
    # TODO would be nice to do this more selectively
    unset SHELL
    unset HOME
    unset buildPhase
    unset buildInputs
    unset PWD
    unset phases
    export | grep -ve "^declare -x PATH=" >> $out

    # TODO add 'pure' parameter to decide whether to inherit PATH
    echo "declare -x PATH=$PATH:\$PATH" >> $out
    echo "\$SHELL" >> $out
    chmod a+x $out
  '';
} // rest)
