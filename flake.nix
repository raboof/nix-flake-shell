{
  description = "A shell";

  inputs = { nixpkgs.url = "path:///home/aengelen/nixpkgs"; };
 
  outputs = { self, nixpkgs }:
  {
    packages.x86_64-linux.hello = nixpkgs.legacyPackages.x86_64-linux.hello;
 
    defaultPackage.x86_64-linux = self.packages.x86_64-linux.hello;

    theShell = 
      let
        pkgs = import nixpkgs { system = "x86_64-linux"; };
        mkShell = pkgs.callPackage ./mkshell.nix {};
      in mkShell {
        buildInputs = [
          (pkgs.sbt.override {
            jre = pkgs.jdk11;
          })
          pkgs.graphviz
          pkgs.multimarkdown
          pkgs.diffoscope
          #nixpkgs-diffoscope.diffoscope
          #pkgs.jetbrains.idea-community
        ];
        JAVA_8_HOME = "${pkgs.jdk8}/lib/openjdk";
        JAVA_11_HOME = "${pkgs.jdk11}/lib/openjdk";
      };
 
    apps.x86_64-linux.myShell = {
      type = "app";
      program = self.theShell.outPath;
    };
  };
}
